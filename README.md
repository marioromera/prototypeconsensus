# prototype-consensus

VUE_APP_<env-mode>_ENV
Must be set in production

### Docker image

Build

```
docker-compose build
```

Run

```
docker rm prototype-consensus
docker run --name prototype-consensus -p 5000:5000  eu.gcr.io/locationip-253711/prototype-consensus:latest
```

Update k8s

```
kubectl set image deployment/prototype-consensus prototype-consensus=mariachisituate/marioromera:prototype-consensus
```

Clean

```
kubectl delete service
gcloud container clusters delete prototype-consensus
```

Reserved ip

````
gcloud compute addresses describe prototype-consensus-ip
```

# Start Project in dev mode
```
lerna exec "yarn start"
````

// TO BE CHANGED

# CONSENSUS (PROTOTYPE)

## SHORT DESCRIPTION

Silently facilitate decision-making. Agreement is achieved through a series of polls, where every connected user is represented, proposals are moderated through algorithms which limitate time and opportunities to speed up discussions and get a fast result for action. Its usage its recommended for real time events where coordination its needed, through the voting, commitment on the proposals bind participants to inmediatly execute any decision taken.

## KEYWORDS:

DECISION-MAKING, PARTICIPATIVE, TOOL, REAL-TIME.

## INTRODUCTION

The internet was born as distributed network of nodes that could maintain the communication, even if some of the nodes would fail(Fault tolerance). The WWW(World Wide Web) was created as another protocol on top of the Internet, this time to set a basis to spread information with the values of Decentralisation, Non-discrimination, Bottom-up design, Universality, and Consensus.
(https://webfoundation.org/about/vision/history-of-the-web/)

In the internet of today, we can still see hints from the ideals attached with the invention of the WWW(World Wide Web), but the reality looks different, the same values which it was founded on, nowadays are exploited for the sake of data exploitation.

Due to the monopolization of the internet, big tech companies and liberalized regulations for them by countries, are transforming the way we conceive and use the internet to a more centralized network.

By having a centralized internet, the information on the network is on risk of being hijacked, obscured or silenced and the access to it can be blocked.
The internet is already subject to the terms of companies and countries, which may or may not allow the users to communicate depending on the content of the messages.

As a response for the control abuse over the network, alternatives appear to maintain the same values that the internet was founded above.

As many examples shown by 2020, many different approaches has been made to re-appropriate the communication and the ways the content is spread for a wide range of use cases, as of data-preservation, uninterrupted communication, and self organization.

## CONCEPT

In 2020, the usage of the internet is increasing unceasingly, the purpose of this project is to reflect over yet another asset of what the internet it is used for, this time using the same tools, for an alternative exploitation model for the sake of understanding each other better through digital mediums.

The speculative value proposition is a web application to achieve commitment between 1+n persons to do anything that the participants would agreed on, based on the techniques used in the digital world, algorithms are designed to achieve different strategies used in horizontal organizations, such as distributed systems, assemblies, and social movements.

All actions are coordinated through a voting platform in which decisions are agreed upon. Once consensus is achieved(depends on the requirement of the group the users, majority wins, total agreement, etc) it is intended that an action take place, it can be an unperformed event, or it can be a collective excersize.

Communitation its autonomous, uncensorable, it allows free speech and its 100% horizontal in terms of participation(every node would receive the same importance).

## TECHNIQUES

Tools used would include:

- Contemporary web technologies such as frameworks, backend and database strategies.
- Security protocols, to achieve secrecy from the users to other users.
- Distributed systems computing?.

## DISCLAIMER

The technologies and concepts used or even only named, would be used speculatively in order to get an idea of what the actual processes could be in a real world enviroment.


# ANOTHER ABSTRACT
An installation where a proof of consensus is shown to be tested in order to get an idea what does it mean to agree with another individuals, the necessities and the relations with the time and space where agreement is resolved. In this installation, a device is prepared to be the medium through which action take place. A commitment between multiple people has to be made in order to interact with the work, the decisions over the course of time are shown as a proof of concept what is the people will, how the mean agreement can be achieved and how it should be establish. 