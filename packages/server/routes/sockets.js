module.exports = io => {
  const signalIntervals = {};
  let socketsIds = [];
  const hbeat = {};
  const updateSockets = ids => {
    socketsIds = ids;
    console.log("connected sockets", socketsIds);
    io.emit("update-peers", socketsIds);
  };

  io.on("connection", socket => {
    socket.emit("connected");

    const id = socket.id;
    socket.emit("id", id);

    if (!socketsIds.includes(id)) updateSockets([...socketsIds, id]);
    socket.emit("initiator", socketsIds.length % 2 === 1);

    socket.on("peers-connected", () =>
      console.log("peers connected p2p works!")
    );
    socket.on("disconnect", function() {
      console.log("disconnecting socket..", id);
      Object.keys(signalIntervals).forEach(i => {
        if (i.includes(id)) {
          clearInterval(signalIntervals[i]);
        }
      });
      clearInterval();
      if (io.sockets.connected[id]) {
        io.sockets.connected[id].disconnect();
      }
      updateSockets(socketsIds.filter(id => socket.id !== id));
    });
    retransmit(socket);
  });

  const retransmit = socket => {
    socket.on("offer", offer => {
      if (!offer) return;
      console.log(socket.id, "send offer to", offer.to);
      send(offer.to, "offer", offer, socket.id);
    });
    socket.on("answer", answer => {
      if (!answer) return;
      console.log(socket.id, "send answer to", answer.to);
      send(answer.to, "answer", answer, socket.id);
    });
  };

  const send = (to, type, data, from) => {
    clearInterval(signalIntervals[`${from}-${to}`]);
    const socket = io.sockets.connected[to];
    if (!socket) return console.error(to, "no socket found!");
    signalIntervals[`${from}-${to}`] = setInterval(() => {
      socket.emit(type, { ...data, from });
    }, 2500);
    setTimeout(() => {
      clearInterval(signalIntervals[`${from}-${to}`]);
    }, 10000);
  };
};
