const express = require("express");
const app = express();
const http = require("http").createServer(app);
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");
var os = require("os");
const SERVER_PORT = process.env.SERVER_PORT || 5000;
const WS_PORT = 8000;
const APP_PORT = 8080;
const io = require("socket.io")(http);

io.listen(WS_PORT);
// Routes
require("./routes/sockets")(io);

app.use(cors());
app.use(bodyParser.json());
app.set("trust proxy", true);

var ifaces = os.networkInterfaces();
address = "no address";
for (var ifname in ifaces) {
  iface = ifaces[ifname];
  for (var i = 0; i < iface.length; i++) {
    var ifaddr = iface[i]["address"];
    //filter ipv6 and localhost addresses
    if (ifaddr.indexOf(".") != -1 && ifaddr.substr(0, 3) != "127") {
      address = ifaddr;
    }
  }
}

if (process.env.NODE_ENV === "production") {
  app.use(express.static("../client/build"));
  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "../client/build", "index.html"));
  });
}

app.get("/", function(req, res) {
  console.log("main route req");
  res.redirect(`http://${address}:${APP_PORT}`);
});

app.get("/address", function(req, res) {
  //send back server address
  console.log("Address requested", address);
  res.send({
    address: `http://${address}`
  });
});

app.listen(SERVER_PORT, () => {
  console.log("Server ip: ", address);
  console.log("server is running on port " + SERVER_PORT);
});
