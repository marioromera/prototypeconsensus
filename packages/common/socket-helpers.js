function findSocketClients(io, namespace, roomId) {
  var res = [],
    // the default namespace is "/"
    ns = io.of(namespace || "/");

  if (ns) {
    for (var id in ns.connected) {
      if (roomId) {
        var index = ns.connected[id].rooms.indexOf(roomId);
        if (index !== -1) {
          res.push(ns.connected[id]);
        }
      } else {
        res.push(ns.connected[id]);
      }
    }
  }
  return res;
}

function getSocketIp(socket) {
  const cip =
    (socket.handshake.headers["x-forwarded-for"] || "").split(",").pop() ||
    socket.request.connection.remoteAddress ||
    socket.handshake.address;
  if (cip === "::1") return "localhost";
  return cip;
}

exports.getIp = getSocketIp;
exports.findSocketClients = findSocketClients;
