export const getApiUrl = () => {
  return process.env.NODE_ENV === "production"
    ? process.env.VUE_APP_PROD_API_URL
    : "http://localhost";
};
