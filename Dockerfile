FROM node:12

WORKDIR /usr/src/app

RUN \
    apt-get update && \
    apt-get install -y git && \
    npm i lerna -g --loglevel notice && \
    rm -rf /var/lib/apt/lists/* 

COPY package.json .
RUN yarn --loglevel notice

COPY packages/client ./packages/client
RUN echo "Content of client"
RUN ls ./packages/client/

COPY packages/server ./packages/server
RUN echo "Content of server"
RUN ls ./packages/server/
COPY ./nginx/ /etc/nginx/conf.d/ 

ENV SERVER_PORT=5000

ENV NODE_ENV=production

# install and build
COPY lerna.json .
RUN lerna bootstrap

RUN cd ./packages/client && yarn build 

EXPOSE 5000

CMD [ "npm", "--prefix", "packages/server", "start" ]